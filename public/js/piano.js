var midiLoaded = false;
MIDI.loadPlugin({
    instrument: "acoustic_grand_piano",
    onsuccess: function() {
    	console.log("MIDI.js Loaded!");
    	midiLoaded = true;
    }
});

var Opcode = {
	JOINROOM: "0",
	NOTEDOWN: "1",
	NOTEUP: "2",
	SUSNOTEUP: "3"
}

var socket = io();
var canvas = document.getElementById("canvas");
var whiteKeys = document.getElementById("whiteKeys");
var blackKeys = document.getElementById("blackKeys");
var engagePedal = document.getElementById("engagePedal");
var connectMidi = document.getElementById("connectMidi");
var dropZone = document.getElementById("dropZone");
var outputMode = true;
var sustain = false;
var sustained = [];
var noteSpeed = 5;
var key;

// Ain't nobody got time to code these in manually
for (var i = 0; i < 15; i++) {
	key = document.createElement("div");
	key.className = "divider";
	canvas.appendChild(key);
}
for (var i = 21; i <= 108; i++) {
	key = document.createElement("div");
	key.id = i.toString();
	attachMouseEvents(key);
	if (i % 12 == 1 || i % 12 == 3 || i % 12 == 6 || i % 12 == 8 || i % 12 == 10) {
		key.className = "blackKey";
		blackKeys.appendChild(key);
	} else {
		key.className = "whiteKey";
		whiteKeys.appendChild(key);
	}
}

/* source == 0: from socket
 * source == 1: from browser/midi file
 * source == 2: from midi device */
function keyDown(keyId, velocity, source) {
	var downKey = document.getElementById(keyId);
	if (midiLoaded && downKey != null) {
		if (outputMode) {
			MIDI.noteOn(0, keyId, velocity, 0);
		} else if (source < 2) {
			WebMidi.outputs[0].send([0x92, keyId, velocity]);
		}
		if (source > 0) {
			if (sustain && !sustained.includes(keyId)) sustained.push(keyId);
			socket.emit(Opcode.NOTEDOWN, String.fromCharCode(keyId, Math.round(velocity)));
		}
		downKey.classList.add("keyDown");
		if (noteSpeed > 0) {
			var clientRect = downKey.getBoundingClientRect();
			var note = document.createElement("div");
			note.className = "noteDown";
			note.style.width = clientRect.width + "px";
			note.style.bottom = clientRect.height + "px";
			note.style.height = "0px";
			downKey.appendChild(note);
		}
	}
}

function keyUp(keyId, source) {
	var upKey = document.getElementById(keyId);
	if (midiLoaded && upKey != null) {
		if (source > 0) {
			if (!sustain) {
				if (outputMode) {
					MIDI.noteOff(0, keyId, 0);
				} else if (source == 1) {
					WebMidi.outputs[0].send([0x82, keyId, 127]);
				}
				socket.emit(Opcode.NOTEUP, String.fromCharCode(keyId)); // unsustained release
			} else {
				socket.emit(Opcode.SUSNOTEUP, String.fromCharCode(keyId)); // sustained release
			}
		} else {
			if (outputMode) {
				MIDI.noteOff(0, keyId, 0);
			} else {
				WebMidi.outputs[0].send([0x82, keyId, 127]);
			}
		}
		upKey.classList.remove("keyDown");
		for (var i = 0; i < upKey.children.length; i++) {
			if (upKey.children[i].className == "noteDown") {
				upKey.children[i].className = "noteUp";
			}
		}
	}
}

function sustainDown() {
	sustain = true;
	engagePedal.style.display = "initial";
}

function sustainUp() {
	if (sustain) {
		sustain = false;
		var sustainEnd = "";
		for (var i = 0; i < sustained.length; i++) {
			if (outputMode) {
				MIDI.noteOff(0, sustained[i], 0);
			}
			sustainEnd += String.fromCharCode(sustained[i]); // sustain end
		}
		socket.emit(Opcode.NOTEUP, sustainEnd); // sustain end
		sustained = [];
		engagePedal.style.display = "none";
	}
}

// Click events on the notes inside the browser
function attachMouseEvents(keyObject) {
	keyObject.onmousedown = function(e) { if (e.buttons == 1) keyDown(e.target.id, 100, 1); };
	keyObject.onmouseenter = function(e) { if (e.buttons == 1) keyDown(e.target.id, 100, 1); };
	keyObject.onmouseup = function(e) { keyUp(e.target.id, 1); };
	keyObject.onmouseleave = function(e) { if (e.buttons == 1) keyUp(e.target.id, 1); };
}

// WebMidi
function enableWebMidi() {
	WebMidi.enable(onSuccess, onFailure);
}

function onSuccess() {
	if (WebMidi.inputs.length > 0) {
		console.log("WebMidi enabled.");
		if (WebMidi.outputs.length > 0) document.getElementById("midiOut").disabled = false;	
		connectMidi.innerHTML = WebMidi.inputs[0].name;
		connectMidi.disabled = true;
		WebMidi.addListener("noteon", function(e) {
			keyDown(e.note.number, e.velocity * 100, 2);
		});
		WebMidi.addListener("noteoff", function(e) {
			keyUp(e.note.number, 2);
		});
		WebMidi.addListener("controlchange", function(e) {
			if (e.value == 127) { // Sustain On
				sustainDown();
			} else if (e.value == 0) { // Sustain Off
				sustainUp();
			}
		});
		WebMidi.addListener("statechange", function(e) {
			if (WebMidi.inputs.length == 0) {
				document.getElementById("browserOut").checked = true;
				document.getElementById("midiOut").disabled = true;
				outputMode = true;
				connectMidi.innerHTML = "Connect MIDI Device";
				connectMidi.disabled = false;
			}
		});
	} else {
		connectMidi.innerHTML = "No MIDI Device detected";
		connectMidi.disabled = true;
		setTimeout(function() {
			connectMidi.innerHTML = "Connect MIDI Device";
			connectMidi.disabled = false;
		}, 1000);
	}
}

function onFailure(err) { 
	connectMidi.innerHTML = "Incompatible Browser";
	connectMidi.disabled = true;
};

enableWebMidi();
connectMidi.onclick = enableWebMidi;

// Sidebar and MIDI File reading functions
function toggleSidebar(isOpened) {
	if (isOpened) {
		document.getElementById("sidebarOpen").style.display = "none";
	} else {
		setTimeout(function() { document.getElementById("sidebarOpen").style.display = "initial"; }, 400);
	}
}

function toggleOutput(isBrowserOut) {
	outputMode = isBrowserOut;
}

function changeNoteSpeed(newSpeed) {
	noteSpeed = parseInt(newSpeed);
	if (noteSpeed == 0) {
		var notesDown = document.getElementsByClassName("noteDown");
		for (var i = 0; i < notesDown.length; i++) {
			notesDown[i].parentNode.removeChild(notesDown[i]);
			i--;
		}
		var notesUp = document.getElementsByClassName("noteUp");
		for (var i = 0; i < notesUp.length; i++) {
			notesUp[i].parentNode.removeChild(notesUp[i]);
			i--;
		}
	}
}

function playMidi(song) {
	var player = MIDI.Player;
	player.loadFile(song, player.start);
	player.addListener(function(note) {
		if (note.message === 144) {
			keyDown(note.note, note.velocity, 1);
		} else if (note.message === 128) {
			keyUp(note.note, 1);
		}
	});
	dropZone.onclick = function() {
		dropZone.innerHTML = "Drop MIDI Files Here";
		player.stop();
	}
}

function readMidi(f) {
	var reader = new FileReader();
	reader.onloadend = function(e) {
		if (e.target.readyState == FileReader.DONE) {
			dropZone.innerHTML = f.name + " (" + f.type + ") - " + f.size + " bytes";
			playMidi(e.target.result);
		} else {
			dropZone.innerHTML = "An error has occurred";
			setTimeout(function() {
				dropZone.innerHTML = "Drop MIDI File Here";
			}, 1000);
		}
	}
	reader.readAsDataURL(f.slice(0, f.size-1));
}

function handleFileSelect(e) {
	e.stopPropagation();
	e.preventDefault();
	if (e.dataTransfer.files[0].type == "audio/mid") {
		dropZone.innerHTML = "Reading MIDI File";
		readMidi(e.dataTransfer.files[0]);
	} else {
		dropZone.innerHTML = "Invalid Format";
		setTimeout(function() {
			dropZone.innerHTML = "Drop MIDI File Here";
		}, 1000);
	}
}

function handleDragOver(e) {
	e.stopPropagation();
	e.preventDefault();
	e.dataTransfer.dropEffect = "copy";
}

dropZone.addEventListener("dragover", handleDragOver, false);
dropZone.addEventListener("drop", handleFileSelect, false);

// Animation
var fps = 0;
function drawNotes(timestamp) {
	var notesDown = document.getElementsByClassName("noteDown");
	for (var i = 0; i < notesDown.length; i++) {
		notesDown[i].style.height = parseInt(notesDown[i].style.height) + noteSpeed + "px";
	}
	var notesUp = document.getElementsByClassName("noteUp");
	for (var i = 0; i < notesUp.length; i++) {
		if (parseInt(notesUp[i].style.bottom) > screen.height) {
			notesUp[i].parentNode.removeChild(notesUp[i]);
			i--;
		} else {
			notesUp[i].style.bottom = parseInt(notesUp[i].style.bottom) + noteSpeed + "px";
		}
	}
	fps++;
	window.requestAnimationFrame(drawNotes);
}

window.requestAnimationFrame(drawNotes);

setInterval(function () {
	document.getElementById("fps").innerHTML = fps + " fps";
	fps = 0;
}, 1000);

// Socket events
socket.on(Opcode.JOINROOM, function(players) {
	if (players == 1) document.getElementById("playerCount").innerHTML = players + " person in this room";
	else document.getElementById("playerCount").innerHTML = players + " people in this room";
});

socket.on(Opcode.NOTEDOWN, function(note) {
	keyDown(note.charCodeAt(0), note.charCodeAt(1), 0);
});

socket.on(Opcode.NOTEUP, function(notes) {
	for (var i = 0; i < notes.length; i++) {
		keyUp(notes.charCodeAt(i), 0);
	}
});

socket.on(Opcode.SUSNOTEUP, function(note) {
	keyUp(note.charCodeAt(0), 0);
});