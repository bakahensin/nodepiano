"use strict";

var port = 3000;

var PianoOpcode = {
	JOINROOM: "0",
	NOTEDOWN: "1",
	NOTEUP: "2",
	SUSNOTEUP: "3"
}

var express = require("express");
var app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);

app.use(express.static("public"));

app.get("/", function(req, res) {
	res.sendFile(__dirname + "/index.html");
});
var pianoPlayerCount = 0;
io.on("connection", function(socket) {
	pianoPlayerCount++;
	io.emit(PianoOpcode.JOINROOM, pianoPlayerCount);

	socket.on(PianoOpcode.NOTEDOWN, function(note) {
		socket.broadcast.emit(PianoOpcode.NOTEDOWN, note);
	});

	socket.on(PianoOpcode.NOTEUP, function(note) {
		socket.broadcast.emit(PianoOpcode.NOTEUP, note);
	});

	socket.on(PianoOpcode.SUSNOTEUP, function(note) {
		socket.broadcast.emit(PianoOpcode.SUSNOTEUP, note);
	});

	socket.on("disconnect", function() {
		pianoPlayerCount--;
		io.emit(PianoOpcode.JOINROOM, pianoPlayerCount);
	});
});

http.listen(port, function() {
	console.log("Listening on *:" + port);
});